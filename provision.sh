#!/usr/bin/env bash 

_installer=apt
_pkgs=(python3-pip python3-dev nginx git)
_project="/home/vagrant"

# install nginx --> flask --> gunicorn
install_tools(){

	for _pkg in ${_pkgs[@]}
		do
			sudo $_installer update -y 
			sudo $_installer install -y  $_pkg
		done
}

get_app_from_git(){
	cd $_project
	git clone https://gitlab.com/eliatiya/nginx.git  #path for app
}

init_app(){
	cd $_project/nginx/app
	sudo pip3 install -r requirements.txt
}

conf_nginx(){
	echo -n '
	server {
		listen 80;
		server_name 10.0.2.15;

		location / {
			include proxy_params;
			proxy_pass http://unix:/home/vagrant/nginx/app/app.sock;
		}
	}' > /etc/nginx/sites-available/app
if [[ $(nginx -t > /dev/null; echo $?) == 0 ]];then
	sudo ln -s /etc/nginx/sites-available/app /etc/nginx/sites-enabled
	sudo systemctl restart nginx
	sudo ufw allow 'Nginx Full'
else
	echo "there is an error with nginx conf"
fi
	
}

systemd_conf(){
echo -n "	
[Unit]
Description=Flask Daemon
After=network.target

[Service]
User=vagrant
Group=www-data
WorkingDirectory=/home/vagrant/app
ExecStart=/usr/local/bin/gunicorn  --workers 3 --bind unix:/home/vagrant/nginx/app/app.sock -m 007 wsgi:app

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/app.service
 sudo systemctl daemon-reload
 sleep 2
 sudo systemctl start app
 sudo systemctl enable --now app
}



#####
#Main -_ - _ -_  -_  -_
####

if [[ $EUID != 0 ]];then
       echo "can NOT provision the server"
else
 	install_tools
	get_app_from_git
	init_app
	systemd_conf
	conf_nginx
	echo "done"
fi

















